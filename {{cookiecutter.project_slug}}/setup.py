import os
import time

from setuptools import find_packages, setup

root_dir = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))


def get_version() -> str:
    version_file = os.path.join(root_dir, 'VERSION_PYPI')
    if not os.path.isfile(version_file):
        return f"0.0.0b{int(time.time())}"
    return open(version_file).read().strip()


# We lock these in the requirements.txt file
install_requires = [
    "python-dotenv>=0.19.0",
    "pyaml>=21.8.3",
]

# Because we do not have a dev dependencies lock file, lock them here
dev_requires = [
    "pip-tools==6.2.0",
    "coverage==5.5",
    "flake8==3.9.2",
    "black==21.7b0",
    "pytest==6.2.4",
    "pytest-mock==3.6.1",
    "pygount==1.2.4",
    "isort==5.9.3",
    "mypy==0.910",
]

{%- set license_classifiers = {
  'MIT license': 'License :: OSI Approved :: MIT License',
  'BSD license': 'License :: OSI Approved :: BSD License',
  'ISC license': 'License :: OSI Approved :: ISC License (ISCL)',
  'Apache Software License 2.0': 'License :: OSI Approved :: Apache Software License',
  'GNU General Public License v3': 'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
} %}

setup(
    name='{{ cookiecutter.project_slug }}',
    description="{{ cookiecutter.project_short_description }}",
    long_description=open(f"{root_dir}/README.md").read().strip(),
    long_description_content_type="text/markdown",
    url="{{ cookiecutter.project_vcs_url }}",
    packages=find_packages(),
    include_package_data=True,
    install_requires=install_requires,
    tests_require=dev_requires,
    extras_require={
      "dev": dev_requires
    },
    python_requires=">={{ cookiecutter.python_version.split('.', 1)[0] }}.0",
    platforms="any",
    version=get_version(),
    author="{{ cookiecutter.full_name.replace('\"', '\\\"') }}",
    author_email='{{ cookiecutter.email }}',
{%- if cookiecutter.open_source_license in license_classifiers %}
    license="{{ cookiecutter.open_source_license }}",
{%- endif %}
    classifiers=[
{%- if cookiecutter.open_source_license in license_classifiers %}
        '{{ license_classifiers[cookiecutter.open_source_license] }}',
{%- endif %}
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: {{ cookiecutter.python_version.rsplit('.', 1)[0] }}",
        "Programming Language :: Python :: {{ cookiecutter.python_version.split('.', 1)[0] }} :: Only",
    ],
)

